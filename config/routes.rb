Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespaces 'cms' do
    resources :students do
      get ':term_id/courses'
    end

    resources :teachers

    # Protect create, update, edit, new so only senior teachers can access them
    # Use a role based apprach for authorisations
    # i.e only the teacher with senior role is approved to do CRUD operations
    resources :courses
    resources :curriculums

    resource :term do
      resource :students do
        get 'enrolled'
        get 'not_enrolled'
      end
    end
  end
end
