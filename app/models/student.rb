class Student
  # Student has only one current term

  has_many :terms # Join model would contain the term-wide remarks for a student

  has_one :curriculum, through: :current_term
  has_many :courses, through: :curriculum # Join model between students and courses would have the course wide grade
  has_many :past_curriculums, through: :past_terms

  validates :only_one_current_term

  def current_curriculum
  end

  def current_term
  end
end
