class Course
  has_many :curricula
  has_many :teachers
end
