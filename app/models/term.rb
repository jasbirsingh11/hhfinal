class Term
  has_one :curriculum
  has_many :students

  scope :current_term, -> () {}
end
